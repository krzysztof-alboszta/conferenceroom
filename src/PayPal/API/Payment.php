<?php

namespace App\PayPal\API;


use App\PayPal\DTO\PaymentCreated;
use Webmozart\Assert\Assert;

class Payment extends AbstractApi implements PayPalApi
{

    private const REL_APPROVAL_URL = 'approval_url';

    /**
     * prepare and send request via PayPal API
     *
     * Assumptions: we receive correct response (no error handling at that stage)
     */
    public function create(array $data): PaymentCreated
    {
        $request = \App\PayPal\Request\Payment::create($data);

        $response = $this->client->getHttpClient()->execute($request);
        $result = $response->result;
        $transactions = $result->transactions;
        $amountData = $transactions[0]->amount;

        return new PaymentCreated(
            $result->id, self::extractApprovalUrl($result->links), $amountData->total, $amountData->currency
        );
    }

    private static function extractApprovalUrl(array $links): string
    {
        $approvalLinks = \array_filter($links, function ($link) {
            return $link->rel === self::REL_APPROVAL_URL;
        });

        Assert::count($approvalLinks, 1);

        return reset($approvalLinks)->href;
    }

}