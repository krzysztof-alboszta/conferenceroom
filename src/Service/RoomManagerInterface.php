<?php

namespace App\Service;

use App\Entity\Room;

interface RoomManagerInterface
{
    public function getRoomByExternalId(int $roomId): Room;

    public function findByName(string $name): ?Room;

    public function createRoom(string $name);

    public function update(Room $room, bool $doSave = false);
}