<?php

namespace App\Service;

use App\Entity\Room;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Webmozart\Assert\Assert;

final class RoomManager implements RoomManagerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getRoomByExternalId(int $roomId): Room
    {
        $room = $this->getRepository()->findOneBy(['externalId' => $roomId]);

        Assert::notNull($room, 'RoomNotFoundException');

        return $room;
    }

    public function findByName(string $name): ?Room
    {
        return $this->getRepository()->findOneBy(['name' => $name]);
    }

    public function createRoom(string $name): Room
    {
        $room = new Room($name);

        $this->entityManager->persist($room);

        return $room;
    }

    public function update(Room $room, bool $doSave = false): void
    {
        $this->entityManager->persist($room);

        if ($doSave) {
            $this->entityManager->flush($room);
        }
    }

    private function getRepository(): EntityRepository
    {
        return $this->entityManager->getRepository(Room::class);
    }
}