<?php

namespace App\PayPal\DTO;

class PaymentCreated
{

    private $paymentId;
    private $approvalUrl;
    private $totalAmount;
    private $currency;

    public function __construct(
        string $paymentId,
        string $approvalUrl,
        float $totalAmount,
        string $currency
    ) {
        $this->paymentId = $paymentId;
        $this->approvalUrl = $approvalUrl;
        $this->totalAmount = $totalAmount;
        $this->currency = $currency;
    }

    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    public function getApprovalUrl(): string
    {
        return $this->approvalUrl;
    }

    public function getTotalAmount(): float
    {
        return $this->totalAmount;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}