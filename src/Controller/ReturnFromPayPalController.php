<?php

namespace App\Controller;

use App\ClickMeeting\API\ClickMeetingApi;
use App\ClickMeeting\ClientInterface;
use App\Entity\ConferenceListener;
use App\Entity\Room;
use App\Service\Conference\ConferenceProvider;
use App\Service\PaymentManager;
use App\Service\RoomManager;
use App\Service\RoomManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Webmozart\Assert\Assert;

/** @Route("/payments") */
class ReturnFromPayPalController extends AbstractController
{

    public const ROUTE_SUCCESS = 'app_payment_success';
    public const ROUTE_FAILURE = 'app_payment_failure';

    public static function getSuccessUrl(UrlGeneratorInterface $urlGenerator): string
    {
        return $urlGenerator->generate(self::ROUTE_SUCCESS, [], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    public static function getFailureUrl(UrlGeneratorInterface $urlGenerator): string
    {
        return $urlGenerator->generate(self::ROUTE_FAILURE, [], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /** @Route("/success", name=ReturnFromPayPalController::ROUTE_SUCCESS) */
    public function success(
        Request $request,
        PaymentManager $paymentManager,
        RoomManagerInterface $roomManager,
        ClientInterface $conferenceApi
    ): Response {

//        $externalId = $request->get('paymentId');
//        $payment = $paymentManager->getPaymentByExternalId($externalId);
        $token = $request->get('token');
        $payment = $paymentManager->getPaymentByToken($token);

        $listener = $payment->getListener();
        $roomId = $this->createConferenceRoom($conferenceApi, $roomManager, $listener);
        $room = $roomManager->getRoomByExternalId($roomId);
        Assert::notNull($room);

        $autoLoginUrl = $this->createAutoLoginUrl($conferenceApi, $room, $listener);

        if ($autoLoginUrl) {
            return $this->redirect($autoLoginUrl);
        }

        $this->addFlash('error', 'Unexpected error');
    }

    /** @Route("/failure", name=ReturnFromPayPalController::ROUTE_FAILURE) */
    public function failure(Request $request): Response
    {
        return new Response('Error in payment occured');
    }

    private function createConferenceRoom(
        ClientInterface $conferenceApi,
        RoomManagerInterface $roomManager,
        ConferenceListener $listener
    ): int {
        /** @var Room $room */
        $room = $listener->getRoom();
        if ($room->getExternalId()) {
            return $room->getExternalId();
        }

        $result = $conferenceApi->conferences()->new(ConferenceProvider::prepareRoomParameters($room->getName()));
        $body = $result->getBody()->getContents();
        $content = \json_decode($body, true);
        Assert::keyExists($content, 'room');
        $roomId = $content['room']['id'];

        $room->setExternalId($roomId);
        $roomManager->update($room, true);

        return $roomId;
    }

    private function createAutoLoginUrl(
        ClientInterface $conferenceApi,
        Room $room,
        ConferenceListener $listener
    ): ?string {
        $result = $conferenceApi->conferences()->createAutoLoginHash($room->getExternalId(), [
            'nickname' => $listener->getNickname(),
            'email' => $listener->getEmail(),
            'password' => 'test',
            'role' => ClickMeetingApi::CLICK_MEETING_ROLE_LISTENER,
        ]);

        $body = $result->getBody()->getContents();
        $content = \json_decode($body, true);
        if (!array_key_exists('autologin_hash', $content)) {
            return null;
        }

        return $conferenceApi->conferences()->prepareAutoLoginUrl($content['autologin_hash']);
    }
}