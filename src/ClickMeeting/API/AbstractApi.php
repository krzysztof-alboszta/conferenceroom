<?php

namespace App\ClickMeeting\API;

use App\ClickMeeting\ClientInterface;
use Psr\Http\Message\ResponseInterface;

class AbstractApi implements ClickMeetingApi
{

    protected $client;
    protected $spaceName;

    public function __construct(ClientInterface $client, string $spaceName)
    {
        $this->client = $client;
        $this->spaceName = $spaceName;
    }

    /** @throws \Http\Client\Exception */
    protected function get(string $path, array $parameters)
    {
        if (\count($parameters) > 0) {
            $path .= '?' . http_build_query($parameters);
        }

        $response = $this->client->getHttpClient()->get($path);

        $body = $response->getBody()->__toString();
        $content = \json_decode($body, true);

        if (JSON_ERROR_NONE !== \json_last_error()) {
            return $body;
        }

        return $content;
    }

    /** @throws \Http\Client\Exception */
    protected function post(string $path, array $parameters): ResponseInterface
    {
        return $this->client->getHttpClient()->post($path, ['Content-type' => 'application/x-www-form-urlencoded'], http_build_query($parameters));
    }

    /** @throws \Http\Client\Exception */
    protected function put(string $path, array $parameters): ResponseInterface
    {
        return $this->client->getHttpClient()->put($path, [], \json_encode($parameters));
    }

    /** @throws \Http\Client\Exception */
    protected function delete(string $path, array $parameters): ResponseInterface
    {
        return $this->client->getHttpClient()->delete($path, [], \json_encode($parameters));
    }
}