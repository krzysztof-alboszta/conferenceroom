<?php

namespace App\PayPal;

use App\PayPal\API\ClickMeetingApi;
use PayPal\Core\PayPalHttpClient;


/**
 * @method API\Payment payments()
 */
interface ClientInterface
{
    public function getHttpClient(): PayPalHttpClient;
}