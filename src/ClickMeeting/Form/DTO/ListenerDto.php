<?php

namespace App\ClickMeeting\Form\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class ListenerDto
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="128")
     */
    private $nickname = '';

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Assert\Length(max="254")
     */
    private $email = '';

    public function getNickname(): string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): void
    {
        $this->nickname = $nickname;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
}