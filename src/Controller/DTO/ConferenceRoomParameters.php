<?php

namespace App\Controller\DTO;

use App\Entity\Room;
use Symfony\Component\Form\FormView;

/**
 * helper dto for using typed references in twig templates
 */
class ConferenceRoomParameters
{
    private $form;
    private $room;
    private $requestedRoomName;

    public function __construct(FormView $form, ?Room $room, string $requestedRoomName)
    {
        $this->form = $form;
        $this->room = $room;
        $this->requestedRoomName = $requestedRoomName;
    }

    public function getForm(): FormView
    {
        return $this->form;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function getRequestedRoomName(): string
    {
        return $this->requestedRoomName;
    }
}