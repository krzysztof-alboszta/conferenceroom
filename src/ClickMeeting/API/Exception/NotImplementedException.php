<?php

namespace App\ClickMeeting\API\Exception;


class NotImplementedException extends \RuntimeException implements ClickMeetingException
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}