<?php

namespace App\Service;

use App\Entity\ConferenceListener;
use App\Entity\Payment;
use App\PayPal\DTO\PaymentCreated;
use Ramsey\Uuid\Uuid;

interface PaymentManagerInterface
{
    public function findPayment(Uuid $id): ?Payment;

    public function getPaymentByExternalId(string $externalId): Payment;

    public function createPayment(Uuid $paymentId, PaymentCreated $dto, ConferenceListener $listener): Payment;

    public function update(Payment $payment, bool $doSave = false): void;
}