<?php

namespace App\Tests\Unit;

use App\PayPal\ClientInterface;
use App\Service\Payment\PaymentProvider;
use App\Service\PaymentManagerInterface;
use PHPUnit\Framework\TestCase;

class PaymentProviderTest extends TestCase
{

    /** @var PaymentProvider */
    private $paymentProvider;

    protected function setUp()
    {
        parent::setUp();

        $payPalClient = $this->createMock(ClientInterface::class);
        $paymentManager = $this->createMock(PaymentManagerInterface::class);

        $this->paymentProvider = new PaymentProvider(
            $payPalClient,
            $paymentManager
        );


    }
}