<?php

namespace App\Service\Payment\Value;

use MyCLabs\Enum\Enum;

/**
 * Enum that represents PayPal payment intent
 * @see https://developer.paypal.com/docs/api/payments/v1/#payment-create-request-body
 *
 * @method static Intent SALE()
 * @method static Intent AUTHORIZE()
 * @method static Intent ORDER()
 */
class Intent extends Enum
{
    private const SALE = 'sale';
    private const AUTHORIZE = 'authorize';
    private const ORDER = 'order';
}