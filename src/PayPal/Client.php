<?php

namespace App\PayPal;

use App\PayPal\API\Payment;
use App\PayPal\API\PayPalApi;
use PayPal\Core\PayPalHttpClient;

/**
 * @method API\Payment payments()
 */
class Client implements ClientInterface
{
    private $environment;

    public function __construct(string $clientId, string $secret, string $mode)
    {
        $this->environment = EnvironmentFactory::create($clientId, $secret, $mode);
    }

    public function api(string $name): PayPalApi
    {
        /** @noinspection DegradedSwitchInspection */
        switch ($name) {
            case 'payments':
                return new Payment($this);
            default:
                throw new \InvalidArgumentException('Invalid API');
        }    }

    public function getHttpClient(): PayPalHttpClient
    {
        return new PayPalHttpClient($this->environment);
    }

    public function __call($name, $args)
    {
        try {
            return $this->api($name);
        } catch (\InvalidArgumentException $e) {
            throw new BadMethodCallException(\sprintf('Undefined method called: "%s"', $name));
        }
    }
}