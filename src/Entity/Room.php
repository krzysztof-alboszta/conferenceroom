<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="ClickMeetingRoom")
 */
class Room
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\Column(name="name", type="string", length=64) */
    private $name;

    /** @ORM\Column(name="room_url", type="string", length=255, nullable=true) */
    private $url;

    /** @ORM\Column(name="external_id", type="bigint", nullable=true) */
    private $externalId;

    public function __construct($name, $url = null, $externalId = null)
    {
        $this->name = $name;
        $this->externalId = $externalId;
        $this->url = $url;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getExternalId(): ?int
    {
        return $this->externalId;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setExternalId(int $roomId): void
    {
        $this->externalId = $roomId;
    }

    public function setUrl(string $autoLoginUrl): void
    {
        $this->url = $autoLoginUrl;
    }

}