<?php

namespace App\ClickMeeting;

use App\ClickMeeting\API\Chat;
use App\ClickMeeting\API\ClickMeetingApi;
use App\ClickMeeting\API\Conference;
use Http\Client\Common\HttpMethodsClient;
use Http\Client\Common\Plugin\BaseUriPlugin;
use Http\Client\Common\Plugin\HeaderDefaultsPlugin;
use Http\Client\Common\PluginClient;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Discovery\UriFactoryDiscovery;

/**
 * @method Api\Conference conferences()
 * @method Api\Chat chats()
 */
final class Client implements ClientInterface
{
    private const X_API_KEY = 'X-Api-Key';
    private $baseUri;
    private $apiKey;
    private $spaceName;

    public function __construct(string $apiKey, string $spaceName, string $baseUri)
    {
        $this->apiKey = $apiKey;
        $this->baseUri = $baseUri;
        $this->spaceName = $spaceName;
    }

    /** @throws \InvalidArgumentException */
    public function api(string $name): ClickMeetingApi
    {
        switch ($name) {
            case 'conferences':
                return new Conference($this, $this->spaceName);
            case 'chats':
                return new Chat($this, $this->spaceName);
            default:
                throw new \InvalidArgumentException('Invalid API');
        }
    }

    public function __call($name, $args)
    {
        try {
            return $this->api($name);
        } catch (\InvalidArgumentException $e) {
            throw new BadMethodCallException(\sprintf('Undefined method called: "%s"', $name));
        }
    }

    public function getHttpClient(): HttpMethodsClient
    {
        $baseUriPlugin = new BaseUriPlugin(
            UriFactoryDiscovery::find()->createUri($this->baseUri),
            [
                'replace' => true,
            ]
        );

        $headerDefaultsPlugin = new HeaderDefaultsPlugin([
            self::X_API_KEY => $this->apiKey
        ]);

        $pluginClient = new PluginClient(
            HttpClientDiscovery::find(),
            [$baseUriPlugin, $headerDefaultsPlugin]
        );

        return new HttpMethodsClient(
            $pluginClient,
            MessageFactoryDiscovery::find()
        );
    }
}