<?php

namespace App\Service\Payment\Value;

use MyCLabs\Enum\Enum;

/**
 * @see https://developer.paypal.com/docs/integration/direct/rest/currency-codes/
 *
 * @method static Currency USD()
 * @method static Currency PLN()
 * @method static Currency PHP()
 */
class Currency extends Enum
{
    private const USD = 'USD';
    private const PLN = 'PLN';
    private const PHP = 'PHP';

    // ...
}