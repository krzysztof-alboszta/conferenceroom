<?php

namespace App\ClickMeeting;

use App\ClickMeeting\API\ClickMeetingApi;
use Http\Client\Common\HttpMethodsClient;

/**
 * @method Api\Conference conferences()
 * @method Api\Chat chats()
 */
interface ClientInterface
{

    public function api(string $name): ClickMeetingApi;

    public function getHttpClient(): HttpMethodsClient;
}