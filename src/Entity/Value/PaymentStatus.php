<?php

namespace App\Entity\Value;

use MyCLabs\Enum\Enum;

/**
 * @method static PaymentStatus AWAITING_APPROVAL
 * @method static PaymentStatus PAID
 * @method static PaymentStatus REFUSED
 * @method static PaymentStatus CANCELLED
 */
final class PaymentStatus extends Enum
{
    private const AWAITING_APPROVAL = 'awaiting_approval';
    private const PAID = 'paid';
    private const CANCELLED = 'cancelled';
    private const REFUSED = 'refused';
}