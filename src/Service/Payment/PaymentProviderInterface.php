<?php

namespace App\Service\Payment;

use App\Entity\ConferenceListener;
use App\Service\Payment\Value\Currency;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

interface PaymentProviderInterface
{
    public static function preparePaymentParameters(
        UrlGeneratorInterface $urlGenerator,
        float $amount,
        Currency $currency
    ): array;

    public function submitPayment(Uuid $paymentId, ConferenceListener $listener, array $parameters): void;
}