<?php

namespace App\Controller;

use App\ClickMeeting\ClientInterface;
use App\ClickMeeting\Form\DTO\ListenerDto;
use App\ClickMeeting\Form\ListenerType;
use App\Controller\DTO\ConferenceRoomParameters;
use App\Entity\ConferenceListener;
use App\Service\Payment\Value\Currency;
use App\Service\PaymentManagerInterface;
use App\Service\Payment\PaymentProvider;
use App\Service\Payment\PaymentProviderInterface;
use App\Service\RoomManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ConferenceController extends AbstractController
{
    /** @Route("/conference/room/{name}"), name="app_conference_room") */
    public function room(
        Request $request,
        string $name,
        RoomManagerInterface $roomManager,
        PaymentManagerInterface $paymentManager,
        PaymentProviderInterface $paymentProvider,
        ClientInterface $client,
        UrlGeneratorInterface $urlGenerator
    ): Response {
        $room = $roomManager->findByName($name);
        $form = $this->createForm(ListenerType::class, new ListenerDto());
        $form->handleRequest($request);

        $paymentId = Uuid::uuid4();

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$room) {
                $room = $roomManager->createRoom($name);
            }
            /** @var ListenerDto $formDto */
            $formDto = $form->getData();
            $listener = new ConferenceListener($formDto->getNickname(), $formDto->getEmail(), $room);
            $this->getDoctrine()->getManager()->persist($listener);
            $this->getDoctrine()->getManager()->flush();

            $paymentParameters = PaymentProvider::preparePaymentParameters(
                $urlGenerator,
                200,
                Currency::PHP()
            );

            $paymentProvider->submitPayment($paymentId, $listener, $paymentParameters);
        }

        $conferenceRoomParameters = new ConferenceRoomParameters(
            $form->createView(),
            $room,
            $name
        );

        return $this->prepareResponseForPayment($paymentId, $conferenceRoomParameters, $paymentManager);
    }

    private function prepareResponseForPayment(
        Uuid $paymentId,
        ConferenceRoomParameters $conferenceRoomParameters,
        PaymentManagerInterface $paymentManager
    ): Response {

        $payment = $paymentManager->findPayment($paymentId);

        if ($payment && $payment->isWaitingForApproval()) {
            return $this->redirect($payment->getApprovalUrl());
        }

        return $this->render('clickmeeting/join.html.twig', [
            'dto' => $conferenceRoomParameters
        ]);
    }
}