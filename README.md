## Paid Conference Room

application integrate PayPal API to pay for access to ClickMeeting room managed via ClickMeeting API





### Used libraries and components

User registration => entry form:

`symfony/form`
`symfony/validator`

Persistence layer => _ClickMeeting_ room data, payments

`doctrine/orm`, 
`doctrine/doctrin-bundle`

`ramsey/uuid`, `ramsey/uuid-doctrine` UUID implementation and integration with doctrine

HTTP interface
`php-http/httplug` abstraction layer for http client
`guzzlehttp/guzzle` HTTP client

