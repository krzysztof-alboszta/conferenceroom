<?php


namespace App\Service\Conference;


class ConferenceProvider
{
    public static function prepareRoomParameters(string $name): array
    {
        return [
            'name' => $name,
            'room_type' => 'meeting',
            'permanent_room' => 0,
            'access_type' => 1,
            'lobby_enabled' => true,
            'lobby_description' => 'My meeting',
            'registration' => [
                'template' => 1,
                'enabled' => true
            ],
            'settings' => [
                'show_on_personal_page' => 1,
                'thank_you_emails_enabled' => 1,
                'connection_tester_enabled' => 1,
                'phonegateway_enabled' => 1,
                'recorder_autostart_enabled' => 1,
                'room_invite_button_enabled' => 1,
                'social_media_sharing_enabled' => 1,
                'connection_status_enabled' => 1,
                'thank_you_page_url' => 'http://127.0.0.1:8000/thank_you.html',
            ],
        ];
    }
}