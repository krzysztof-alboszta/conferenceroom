<?php

namespace App\ClickMeeting\API\Conference;

use App\ClickMeeting\API\AbstractApi;
use App\ClickMeeting\API\Exception\NotImplementedException;

class Session extends AbstractApi
{

    public function all(int $roomId): array
    {
        return $this->get('/conferences/' . rawurlencode((string)$roomId) . '/sessions', []);
    }

    public function show(int $roomId, int $sessionId): array
    {
        return $this->get(
            '/conferences/' . rawurlencode((string)$roomId) . '/sessions/' . rawurlencode((string)$sessionId),
            []
        );
    }

    public function listAttendees(): array
    {
        throw new NotImplementedException('Method not implemented yet');
    }

    public function generatePDF(string $lang)
    {
        throw new NotImplementedException('Method not implemented yet');
    }
}