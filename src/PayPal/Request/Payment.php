<?php

namespace App\PayPal\Request;

use BraintreeHttp\HttpRequest;
use PayPal\v1\Payments\PaymentCreateRequest;
use PayPal\v1\Payments\PaymentGetRequest;
use PayPal\v1\Payments\PaymentListRequest;
use PayPal\v1\Payments\PaymentUpdateRequest;

class Payment
{
    public static function create(array $body): HttpRequest
    {
        $request = new PaymentCreateRequest();
        $request->body = $body;

        return $request;
    }

    public static function get(int $paymentId): HttpRequest
    {
        return new PaymentGetRequest($paymentId);
    }

    public static function all(): HttpRequest
    {
        return new PaymentListRequest();
    }

    public static function update(int $paymentId, $data): HttpRequest
    {
        $request = new PaymentUpdateRequest($paymentId);
        $request->body = $data;

        return $request;
    }
}