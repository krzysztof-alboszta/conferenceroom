<?php

namespace App\ClickMeeting\API\Exception;

/**
 * Marker interface for all ClickMeeting API exceptions
 */
interface ClickMeetingException
{

}