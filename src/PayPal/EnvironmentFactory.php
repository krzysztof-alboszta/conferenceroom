<?php

namespace App\PayPal;

use PayPal\Core\PayPalEnvironment;
use PayPal\Core\SandboxEnvironment;
use PayPal\Core\ProductionEnvironment;

class EnvironmentFactory
{
    public static function create(string $clientId, string $secret, string $mode): PayPalEnvironment
    {
        switch ($mode) {
            case 'sandbox':
                return new SandboxEnvironment($clientId, $secret);
            case 'production':
                return new ProductionEnvironment($clientId, $secret);
            default:
                throw new \InvalidArgumentException('Invalid PayPal environment');
        }
    }
}