<?php

namespace App\Tests\ClickMeeting;

use App\ClickMeeting\API\Chat;
use App\ClickMeeting\Client;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\Strategy\MockClientStrategy;
use PHPUnit\Framework\TestCase;
use App\ClickMeeting\API\Conference;

class ClientTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        HttpClientDiscovery::prependStrategy(MockClientStrategy::class);
    }

    public function test_getHttpClient(): void
    {
        $client = new Client('API-KEY', 'test' , 'http://localhost/api');
        $this->assertInstanceOf(\Http\Client\HttpClient::class, $client->getHttpClient());
    }

    public function test_getApiInstance(): void
    {
        $client = new Client('API-KEY', 'test' , 'http://localhost/api');

        $api = $client->api('conferences');
        self::assertInstanceOf(Conference::class, $api);

        $api = $client->chats();
        self::assertInstanceOf(Chat::class, $api);
    }

    public function test_throwsException_onUnknownApi(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $client = new Client('API-KEY', 'test' , 'http://localhost/api');
        $client->api('unknown_api');
    }
}
