<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity that represent ClickMeeting participant registered to particular room
 *
 * @ORM\Entity
 * @ORM\Table(name="ClickMeetingListener")
 */
class ConferenceListener
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\Column(name="nickname", type="string", length=128) */
    private $nickname;

    /** @ORM\Column(name="email", type="string", length=254) */
    private $email;

    /** @ORM\Column(name="external_id", type="bigint", nullable=true) */
    private $registrationId;

    /** @ORM\ManyToOne(targetEntity="Room") */
    private $room;

    public function __construct(string $nickname, string $email, Room $room)
    {
        $this->nickname = $nickname;
        $this->email = $email;
        $this->room = $room;
    }

    public function setReferenceId(int $id): void
    {

    }

    /**
     * @return mixed
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getNickname(): string
    {
        return $this->nickname;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getRegistrationId(): int
    {
        return $this->registrationId;
    }

    public function getRoom(): Room
    {
        return $this->room;
    }

}