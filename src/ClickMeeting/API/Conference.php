<?php

namespace App\ClickMeeting\API;


use App\ClickMeeting\API\Conference\Session;
use Psr\Http\Message\ResponseInterface;

class Conference extends AbstractApi
{

    public function allActive(): array
    {
        return $this->get('/conferences/active', []);
    }

    // TODO: add pagination parameter (should be handled globally)
    public function allInactive(): array
    {
        return $this->get('/conferences/inactive', []);
    }

    public function new($conference): ResponseInterface
    {
        return $this->post('/conferences', $conference);
    }

    public function sessions(): ClickMeetingApi
    {
        return new Session($this->client, 'test');
    }

    public function createAutoLoginHash(int $roomId, array $userData): ResponseInterface
    {
        return $this->post(\sprintf('/conferences/%s/room/autologin_hash', $roomId), $userData);
    }

    public function prepareAutoLoginUrl(string $hash): string
    {
        return \sprintf('https://%s.clickmeeting.com/room_url?l=%s', $this->spaceName, $hash);

    }
}