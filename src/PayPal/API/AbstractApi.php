<?php

namespace App\PayPal\API;

use App\PayPal\ClientInterface;

class AbstractApi
{
    protected $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

}