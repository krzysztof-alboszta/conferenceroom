<?php

namespace App\Entity;

use App\Entity\Value\PaymentStatus;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Entity that represent payment details done by a conference listener
 *
 * @ORM\Entity
 * @ORM\Table(name="Payment")
 */
class Payment
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="uuid")
     */
    private $id;
    /** @ORM\Column(name="external_id", type="string") */
    private $externalId;
    /** @ORM\Column(name="approval_url", type="string") */
    private $approvalUrl;
    /** @ORM\Column(name="amount", type="decimal", precision=10, scale=2) */
    private $amount;
    /** @ORM\Column(name="status", type="string") */
    private $status;
    /** @ORM\Column(name="currency", type="string") */
    private $currency;
    /** @ORM\ManyToOne(targetEntity="ConferenceListener") */
    private $listener;

    public function __construct(
        Uuid $id,
        ConferenceListener $listener,
        string $providerId,
        string $approvalUrl,
        PaymentStatus $status,
        float $amount,
        string $currency
    ) {
        $this->id = $id;
        $this->amount = $amount;
        $this->status = $status;
        $this->listener = $listener;
        $this->externalId = $providerId;
        $this->amount = $amount;
        $this->currency = $currency;
        $this->approvalUrl = $approvalUrl;
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getApprovalUrl(): string
    {
        return $this->approvalUrl;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function isWaitingForApproval(): bool
    {
        return $this->status->equals(PaymentStatus::AWAITING_APPROVAL());
    }

    public function getListener(): ConferenceListener
    {
        return $this->listener;
    }
}