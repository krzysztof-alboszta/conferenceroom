<?php

namespace App\ClickMeeting\API;

/**
 * Marker interface
 */
interface ClickMeetingApi
{

    public const CLICK_MEETING_ROLE_LISTENER = 'listener';
}