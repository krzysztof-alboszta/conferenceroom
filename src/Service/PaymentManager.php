<?php

namespace App\Service;


use App\Entity\ConferenceListener;
use App\Entity\Payment;
use App\Entity\Value\PaymentStatus;
use App\PayPal\DTO\PaymentCreated;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Ramsey\Uuid\Uuid;
use Webmozart\Assert\Assert;

final class PaymentManager implements PaymentManagerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findPayment(Uuid $id): ?Payment
    {
        return $this->getRepository()->find($id->toString());
    }

    /**
     * Workaround to bind payment with token received from PayPal via the success_url
     */
    public function getPaymentByToken(string $token): Payment
    {
        $qb = $this->getRepository()->createQueryBuilder('Payment');
        $qb->where('Payment.approvalUrl LIKE :token')
            ->setParameter('token', '%token=' . $token . '%');

        $payment = $qb->getQuery()->getOneOrNullResult();

        Assert::notNull($payment, 'PaymentNotFoundException');

        return $payment;
    }

    public function getPaymentByExternalId(string $externalId): Payment
    {
        $payment = $this->getRepository()->findOneBy(['externalId' => $externalId]);

        Assert::notNull($payment, 'PaymentNotFoundException');

        return $payment;
    }

    public function createPayment(Uuid $paymentId, PaymentCreated $dto, ConferenceListener $listener): Payment
    {
        $payment = new Payment(
            $paymentId, $listener, $dto->getPaymentId(), $dto->getApprovalUrl(), PaymentStatus::AWAITING_APPROVAL(),
            $dto->getTotalAmount(), $dto->getCurrency()
        );

        return $payment;
    }

    public function update(Payment $payment, bool $doSave = false): void
    {
        $this->entityManager->persist($payment);

        if ($doSave) {
            $this->entityManager->flush($payment);
        }
    }

    private function getRepository(): EntityRepository
    {
        return $this->entityManager->getRepository(Payment::class);
    }
}