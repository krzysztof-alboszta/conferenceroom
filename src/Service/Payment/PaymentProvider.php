<?php

namespace App\Service\Payment;


use App\Controller\ReturnFromPayPalController;
use App\Entity\ConferenceListener;
use App\PayPal\ClientInterface;
use App\Service\Payment\Value\Currency;
use App\Service\Payment\Value\Intent;
use App\Service\PaymentManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class PaymentProvider implements PaymentProviderInterface
{
    private const DEFAULT_PAYMENT_METHOD = 'paypal';

    private $payPalClient;
    private $paymentManager;

    public function __construct(
        ClientInterface $payPalClient,
        PaymentManagerInterface $paymentManager
    ) {
        $this->payPalClient = $payPalClient;
        $this->paymentManager = $paymentManager;
    }

    public static function preparePaymentParameters(
        UrlGeneratorInterface $urlGenerator,
        float $amount,
        Currency $currency
    ): array {
        $redirectUrls = [
            'cancel_url' => ReturnFromPayPalController::getSuccessUrl($urlGenerator),
            'return_url' => ReturnFromPayPalController::getFailureUrl($urlGenerator),
        ];

        return [
            'intent' => Intent::SALE()->getValue(),
            'payer' => [
                'payment_method' => self::DEFAULT_PAYMENT_METHOD,
            ],
            'transactions' => [
                [
                    'amount' => [
                        'total' => $amount,
                        'currency' => $currency->getValue(),
                    ]
                ]
            ],
            'redirect_urls' => $redirectUrls,
        ];
    }

    public function submitPayment(Uuid $paymentId, ConferenceListener $listener, array $parameters): void
    {
        $paypalPayment = $this->payPalClient->payments()->create($parameters);

        $payment = $this->paymentManager->createPayment($paymentId, $paypalPayment, $listener);
        $this->paymentManager->update($payment, true);
    }
}